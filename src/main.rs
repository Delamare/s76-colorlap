use anyhow::{Context, Result};
use clap::Parser;
use glam::{IVec3, Vec3};
use lazy_static::lazy_static;
use log::{debug, info};
use rand::Rng;
use std::io::Write;
use std::path::PathBuf;
use std::time::{Duration, Instant};

#[derive(Parser, Debug)]
struct Settings {
    #[clap(
        short,
        long,
        default_value = "/sys/class/leds/system76_acpi::kbd_backlight/color"
    )]
    /// Path to the System76 keyboard color file
    file: PathBuf,

    #[clap(short, long, default_value = "2.0")]
    /// Time in seconds to transition between random color values
    speed: f64,

    #[clap(short, long, default_value = "14.0")]
    /// Frequency of color updates in Hz (fps), therefore, also rate of file writes.
    rate: f32,
}

lazy_static! {
    static ref SETTINGS: Settings = Settings::parse();
}

fn random_color() -> Vec3 {
    let mut rng = rand::thread_rng();
    Vec3::new(
        rng.gen_range(0.0..1.0),
        rng.gen_range(0.0..1.0),
        rng.gen_range(0.0..1.0),
    )
}

fn vec_to_8bit(color: Vec3) -> IVec3 {
    (color * Vec3::new(255.0, 255.0, 255.0)).as_ivec3()
}

fn vec_to_string(color: Vec3) -> String {
    let color = vec_to_8bit(color);
    format!("{:02X}{:02X}{:02X}", color.x, color.y, color.z)
}

fn set_color(color: Vec3) -> Result<()> {
    let color_string = vec_to_string(color);
    std::fs::File::create(&SETTINGS.file)
        .context("Failed to open System76 keyboard color file for writing")?
        .write_all(color_string.as_bytes())
        .context("Failed to write new color value to the System76 keyboard color file")?;
    Ok(())
}

fn read_color() -> Result<Vec3> {
    let error_str = "Failed to parse System76 keyboard color file";
    let color_string = std::fs::read_to_string(&SETTINGS.file)
        .context("Failed to read System76 keyboard color file")?;
    let r = &color_string.get(0..2).context(error_str)?;
    let g = &color_string.get(2..4).context(error_str)?;
    let b = &color_string.get(4..6).context(error_str)?;
    Ok(Vec3::new(
        i32::from_str_radix(r, 16).context(error_str)? as f32 / 255.0,
        i32::from_str_radix(g, 16).context(error_str)? as f32 / 255.0,
        i32::from_str_radix(b, 16).context(error_str)? as f32 / 255.0,
    ))
}

fn setup_logger() {
    let mut builder = env_logger::Builder::from_default_env();
    // I like INFO being the default log level instead of WARN
    if std::env::var("RUST_LOG").is_err() {
        builder.filter_level(log::LevelFilter::Info);
    }
    builder.init();
}

fn main() -> Result<()> {
    setup_logger();
    info!("\n{:#?}", *SETTINGS);

    let sleep_time = Duration::from_secs_f32(1.0 / SETTINGS.rate);

    let mut target_color = random_color();
    let mut old_color = read_color()?;
    let mut old_color_time = Instant::now();
    loop {
        let percent_complete = (Instant::now() - old_color_time).as_secs_f64() / SETTINGS.speed;

        if percent_complete > 1.0 {
            old_color = target_color;
            target_color = random_color();
            debug!("New target color: {}", vec_to_string(target_color));
            old_color_time = Instant::now();
        } else {
            set_color(old_color.lerp(target_color, percent_complete as f32))?;
        }

        // I know that if I wanted to be proper, I'd time the above, subtract, etc, but this is good enough
        std::thread::sleep(sleep_time);
    }
}
