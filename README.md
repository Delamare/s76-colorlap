# s76-colorlap  
A simple application that plays a pleasing color animation on your System76 laptop keyboard!  

## What animation?  
The app lerps the keyboard color to a new color over a period of `--speed` seconds every `--speed` seconds.  

Or more thoroughly put:  

A random color is chosen every `--speed` seconds and the application performs a linear interpolation between the previous color and the newly generated one.  
The interpolation takes `--speed` seconds.  
The interpolated color is written to the control `--file` at a rate of `--rate` Hz.
